# MicroBit Morse Code

This program was written by Matthew Cane for IoT MiroBit Challenge 2

## How to run

1. Run `bash nano source/settings.h`
2. Change the MESSAGE variable in settings.h to the message to be sent
3. Run `yt clean`
4. Run `yt build`
5. Run `cd build/bbc-microbit-classic-gcc/source`
6. Run `cp microbit-morse-code-combined.hex /media/MICROBIT`
7. Repeat step 5 for the second MicroBit

## How to use

1. Connect MicroBit 1 and MicroBit 2 on pin 0
2. Press button A on MicroBit 1 to enter Rx mode
3. Press button B on MicroBit 2 to enter Tx mode and begin transmission
4. When the transmission is complete, MicroBit 1 will display the message

## Protocol Information

Each character is represented by 1 byte.
Each bit length is dictated by the value in setting.h.
Each character follows ASCII code.
Only ASCII printable characters are permitted, between 0x20 and 0x7F. No control characters or extended codes are permitted.
To signify the start of transmission, the sender will transmit 0xFF.
To signify the end of transmission, the sender will transmit 0x0.
