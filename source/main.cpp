#include "MicroBit.h"
#include "rx.h"
#include "tx.h"

using namespace std;
MicroBit uBit;

void rxMode() {
    rx recieve;
    ManagedString message = "FAIL";
    uBit.display.print("L");
    if (recieve.listen()) {
        uBit.display.print("R");
        message = recieve.record();
    }
    uBit.display.scroll("MESSAGE:");
    uBit.display.scroll(message);
}

void txMode() {
    tx transmit;
    ManagedString message = "";
    uBit.display.print("S");
    message = saveMessage();
    uBit.display.print("T");
    transmit.send(message);
    uBit.display.scroll("MESSAGE SENT");
}

int main() {
    uBit.init();

    while(1) {
        uBit.display.print("M");
        if (uBit.buttonA.isPressed()) {
            rxMode();
        }
        if (uBit.buttonB.isPressed()) {
            txMode();
        }
    }
}
