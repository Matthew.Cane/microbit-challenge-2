#include "rx.h"

bool rx::listen() {
    for (int i = 0; i < 8; i++) {
        if (uBit.io.P0.getDigitalValue() == 0) { i = 0; }
        uBit.sleep(BIT_LENGTH);
    }
    return true;
}

ManagedString rx::record() {
    //ManagedString message = "";
    MicroBitSerial serial(USBTX, USBRX); //DEBUG
    char message[32];
    strcpy(message, "\0");
    char character = 0x1;
    int bit = 0;
    uBit.io.P1.setDigitalValue(0);

    while (character != 0x0) {
        for (int i = 0; i < 8; i++) {
            character = 0x0;
            bit = uBit.io.P0.getDigitalValue();
            serial.send(bit); //DEBUG
            //uBit.display.print(bit); //DEBUG
            character |= (bit << i);
            uBit.sleep(BIT_LENGTH);
        }
        append(message, character);
        //serial.sendChar(character); //DEBUG
        //serial.send("\r\n"); //DEBUG
    }

    return message;
}

void rx::append(char* string, char character) {
    int length = strlen(string);
    string[length] = character;
    string[length+1] = '\0';
}
