#include "MicroBit.h"
#include "settings.h"
#include "string.h"
extern MicroBit uBit;

class rx {
public:
    bool listen();
    ManagedString record();
    void append(char* string, char character);
};
