#include "tx.h"

void tx::send(ManagedString message) {
    bool bit;
    int messageLength = message.length();
    startOfTransmission();
    for (int i = 0; i < messageLength; i++) {
        char x = message.charAt(i);
        for (int y = 0; y < 8; y++) {
            bit = (x >> y) & 0x1;
            uBit.io.P0.setDigitalValue(bit);
            //uBit.display.print(bit); //DEBUG
            uBit.sleep(BIT_LENGTH);
        }
    }
    endOfTransmission();
}

ManagedString tx::saveMessage() {
    bool listening = true;
    ManagedString morse = "";

    while (listening) {
        uBit.sleep(MORSE_UNIT);
        if (uBit.buttonB.isPressed) { // dot
            morse = morse + ".";
        }
        uBit.sleep(MORSE_UNIT);
        if (uBit.buttonB.isPressed) { // dash
            morse = morse + "-";
        }
        uBit.sleep(MORSE_UNIT);
        if (uBit.buttonB.isPressed) { // space
            morse = morse + "-";
        }


}

void tx::startOfTransmission(){
    uBit.io.P0.setDigitalValue(1);
    //uBit.display.print(1); //DEBUG
    uBit.sleep(BYTE_LENGTH);
}

void tx::endOfTransmission(){
    uBit.io.P0.setDigitalValue(0);
    //uBit.display.print('0'); //DEBUG
    uBit.sleep(BYTE_LENGTH);
}
