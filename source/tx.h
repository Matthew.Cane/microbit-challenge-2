#include "MicroBit.h"
#include "settings.h"
extern MicroBit uBit;

class tx {
public:
    void send(ManagedString message);
    ManagedString saveMessage();
    void startOfTransmission();
    void endOfTransmission();
};
